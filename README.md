# Flyer Data Extractor by A.Basile - Brousers

### Installing Tesseract-OCR
## FOR WINDOWS USERS ONLY:
1. go to https://github.com/UB-Mannheim/tesseract/wiki
2. choose your installer depending on OS
3. execute the .exe file 
The instalation folder is usually: C:\Users\{user}\AppData\Local\Programs\Tesseract-OCR
FOR MAC USER:
1.on terminal :brew install tesseract
2. and then :brew install tesseract-lang  # Option 2: for all language packs
The instalation folder is usually:/usr/local/bin/tesseract                        $ which tesseract

4. insert the previous path + \tesseract inside the variable pytesseract.pytesseract.tesseract_cmd

        pytesseract.pytesseract.tesseract_cmd = (r"C:\Users\andre\AppData\Local\Programs\Tesseract-OCR\tesseract)

5. install spacy using the terminal

        python -m spacy download en_core_web_sm
        python -m spacy download it_core_news_sm

6. RUN THE MAIN FILE INSIDE IT THERE'S A LIST OF URLS , I JUST ADD ONE EXAMPLE DEMO FOR YOU TO RUN ,
        IT ALSO CREATES A JSON , WITH THE URL OF THE IMAGE AND THE CAPTION FOR EACH POST

## For Linux users
1. Open Terminal
2. Install tesseract-orc

        sudo apt install tesseract-ocr
