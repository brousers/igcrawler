import json
import re
import time
import requests
from bs4 import BeautifulSoup
import random
import string
from ocr import data2event
def extract_values(html_content):
    # Regular expressions based on the specific patterns provided for each value
    patterns = {
        "csrf_token": re.compile(r'\\["\']csrf_token\\["\']\s*:\s*\\["\'](.*?)\\["\']'),
        "_nc_x": re.compile(r'_nc_x=([a-zA-Z0-9-]+)'),
        "appid": re.compile(r'X-IG-App-ID["\']?\s*:\s*["\']?(\d+)'),
        "device_id": re.compile(r'"device_id"\s*:\s*"([a-zA-Z0-9-]+)"')
    }

    # Dictionary to hold the results
    extracted_values = {}

    # Search for each value in the HTML content using its specific pattern
    for key, pattern in patterns.items():
        search_result = pattern.search(html_content)
        if search_result:
            extracted_values[key] = search_result.group(1)
        else:
            extracted_values[key] = None  # If the pattern doesn't match, the value is not found

    return extracted_values

def __makeFirstRequest__(url: str) -> str:
    headers = {
    'Host': 'www.instagram.com',
    'Viewport-Width': '1680',
    'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
    'Sec-Ch-Ua-Mobile': '?0',
    'Sec-Ch-Ua-Platform': '"macOS"',
    'Sec-Ch-Prefers-Color-Scheme': 'light',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-User': '?1',
    'Sec-Fetch-Dest': 'document',
    # 'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    'Connection': 'close',
}
    tries = 0
    while tries < 5:
        try:
            # if url ends with / just remove it
            if url[-1] == '/':
                url = url[:-1]
            response = requests.get(url, headers=headers, timeout=3, verify=False)
            return response.text
        except Exception as e:
            print('Errore nella richiesta: ' +
                  str(e)+'\nTentativo: '+str(tries))
            tries += 1
            time.sleep(1)
    return None




# Function to recursively search for keys in a nested dictionary and retrieve their values.
def find_items(id, json_repr):
    paths = []

    def _find_items_helper(obj, path):
        if isinstance(obj, dict):
            for key, value in obj.items():
                new_path = path + [key] if path else [key]
                if key == id and isinstance(value, str):  # assuming the item is always a string (URL)
                    paths.append((value, new_path))  # store the item with its path
                elif isinstance(value, (dict, list)):
                    _find_items_helper(value, new_path)
        elif isinstance(obj, list):
            for index, item in enumerate(obj):
                new_path = path + [str(index)]
                _find_items_helper(item, new_path)

    _find_items_helper(json_repr, [])
    return paths

def find_image_url(found_items):
   
    images=[]
    i=0
    for url, path in found_items:
    
        if path[0]=='data':
                if path[1]=='user':
                    
                    if path[2]=='edge_owner_to_timeline_media':
                        if path[3]=='edges':
                            if path[4]==str(i):
                            
                                if path[5]=='node':
                                    if path[6]=='display_url':
                                     
                                        images.append(url)
                                        i=i+1
    return images
def find_captions(found_items):
    captions=[]
    i=0
    for url, path in found_items:
        if path[0]=='data':
                if path[1]=='user':
                    if path[2]=='edge_owner_to_timeline_media':
                        if path[3]=='edges':
                            if path[4]==str(i):
                                if path[5]=='node':   
                                    if path[6]=='edge_media_to_caption':   
                                        if path[7]=='edges':
                                            if path[8]==str(0):
                                                if path[9]=='node':
                                                    if path[10]=='text':
                                                       
                                                        i=i+1
                                                        captions.append(url)
    return captions 
def __makeLastRequest__(xcstokenparam: str,igappidparam: str,deviceidparam: str,refeerparam: str ) -> str:
    x_midparam = '7hjqrgjhrgh91yt3poo1p4uj9u18hmo41fy4of2nh702chbw5zb'
    xasbdidparam='129477'
    headers = {
    'Host': 'www.instagram.com',
    'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
    'X-Mid': x_midparam,
    'X-Ig-Www-Claim': '0',
    'Sec-Ch-Ua-Platform-Version': '""',
    'X-Requested-With': 'XMLHttpRequest',
    'X-Web-Device-Id': deviceidparam,
    'Dpr': '2',
    'X-Csrftoken': xcstokenparam,
    'Sec-Ch-Ua-Model': '""',
    'Sec-Ch-Ua-Platform': '"macOS"',
    'X-Ig-App-Id': igappidparam,
    'Sec-Ch-Prefers-Color-Scheme': 'light',
    'Sec-Ch-Ua-Mobile': '?0',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
    'Viewport-Width': '1680',
    'Accept': '*/*',
    'X-Asbd-Id': xasbdidparam,
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Dest': 'empty',
    'Referer': refeerparam,
    # 'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
}
    #for ex from this https://www.instagram.com/sound_department/ we get sound_department
    usernameparam=refeerparam.split('/')[3]
    

    params = {
    'username': usernameparam,
}

    url='https://www.instagram.com/api/v1/users/web_profile_info/'
    tries = 0
    while tries < 5:
        try:
            # if url ends with / just remove it
            if url[-1] == '/':
                url = url[:-1]
            response = requests.get(url, headers=headers, timeout=3, verify=False, params=params)
            return response.text
        except Exception as e:
            print('Errore nella richiesta: ' +
                  str(e)+'\nTentativo: '+str(tries))
            tries += 1
            time.sleep(1)
    return None



def extract_data(content,url):

# Try to parse the string as JSON.
    try:
        data = json.loads(content)
    except json.JSONDecodeError:
    # The content is not valid JSON.
        data = None

    # Search for specific keys that are known to contain URLs or media
    # Define the keys you're looking for
    keys_to_search = ['video_url', 'display_url', 'thumbnail_src', 'profile_pic_url','text']

# Find the items and their paths in the JSON
    found_items = []
    for key in keys_to_search:
        found_items.extend(find_items(key, data))

    # Download the items
  
    images=[]
    captions=[]
    images=find_image_url(found_items)
    captions=find_captions(found_items)
    #create a list of objects named ig_data
    #which is image and description
    list_of_objects=[]
    list_of_json=[]
    for img_url, caption in zip(images, captions):
        json_data = {
            'image': img_url,
            'description': caption
        }
        list_of_json.append(json_data)
    
    for i in range(len(images)):
          
       
        obj={}
       
        data_image=None
        data_image=download_image_bytes(images[i])
        
        print(captions[i])
        print(images[i])
        obj['image']=data_image
        obj['description']=captions[i]
       
        list_of_objects.append(obj)
    #create a list of tuples (image,caption)
    #transform url https://www.instagram.com/sound_department/' in sound_department
    #and use it as a name for the json file
    name=url.split('/')[3]
    #transform the list of objects in a json file
    
   
    with open(name+'.json', 'w') as outfile:
        json.dump((list_of_json,), outfile,indent=2)
   
    return list_of_objects


def download_image_bytes(url):
    # Send a request to download the content at the URL
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        # If request is successful, return the binary data
        return response.content
    else:
        return None


if __name__ == "__main__":
    
    
    list_urls=[]
    list_of_data=[]
    list_urls.append('https://www.instagram.com/sound_department/')
    for url in list_urls:
        html_content=__makeFirstRequest__(url)
        extracted_values = extract_values(html_content)
        device_id=extracted_values['device_id']
        appid=extracted_values['appid']
        csrf_token=extracted_values['csrf_token']
        content=__makeLastRequest__(csrf_token,appid,device_id,url)
        
        data=extract_data(content,url)
        i=1
        for post in data:
            print("post numero: "+str(i))
            data2event(post)
            i=i+1
 
  
        
        
    

    
    
    
   
