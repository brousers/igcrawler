import pytesseract
from PIL import Image
import spacy
from io import BytesIO
from sys import platform

# nlp = spacy.load("it_core_news_sm")
nlp = spacy.load("en_core_web_sm")


if platform != "linux":
    pytesseract.pytesseract.tesseract_cmd = r"/usr/local/bin/tesseract"


def image2text(image):
    text = pytesseract.image_to_string(Image.open(BytesIO((image))))
    return text


def text_entities(text):
    doc = nlp(text)
    return doc


def image2entites(image, description):
    text = image2text(image)
    print("******************+***************")
    print("DESCRIPTION:")
    print(description)
    print("******************+***************")
    print("text found in image:")
    print(text)
    print("******************+***************")
    text = text + " " + description
    print("******************+***************")
    labeled_entities = text_entities(text)
    return labeled_entities


def data2event(data):
    image = data["image"]
    description = data["description"]
    entities_dict = image2entites(image, description)

    for entity in entities_dict.ents:
        print(
            f"Entity: {entity}, Label: {entity.label_}, {spacy.explain(entity.label_)}"
        )
    # now we need to get the appropriate entitities to recognize a caption+image as an event,
    # hmmm...
    return entities_dict
