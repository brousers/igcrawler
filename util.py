
import random
import string
import time
from bs4 import BeautifulSoup
import requests
import re
#I CURRENTLY DON'T USE THESE FUNCTIONS BUT I KEEP THEM HERE FOR FUTURE USE

# Function to generate a random 'X-Mid' value (assuming it's a random string which follows a certain pattern)
def generate_random_x_mid():
    # Modify this function based on the exact pattern of 'X-Mid' you've observed
    length = 56  # based on the example you provided
    characters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(characters) for i in range(length))
    return random_string



def __makeAsbdRequest__(url: str,ncx_param) -> str:
    headers = {
    'Host': 'static.cdninstagram.com',
    'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
    'Origin': 'https://www.instagram.com',
    'Sec-Ch-Ua-Mobile': '?0',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
    'Sec-Ch-Ua-Platform': '"macOS"',
    'Accept': '*/*',
    'Sec-Fetch-Site': 'cross-site',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Dest': 'script',
    'Referer': 'https://www.instagram.com/',
    # 'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    'Connection': 'close',
}
    params = {
    '_nc_x': ncx_param
}
    tries = 0
    while tries < 5:
        try:
            # if url ends with / just remove it
            if url[-1] == '/':
                url = url[:-1]
            response = requests.get(url, headers=headers, timeout=3, verify=False,params=params)
            return response.text
        except Exception as e:
            print('Errore nella richiesta: ' +
                  str(e)+'\nTentativo: '+str(tries))
            tries += 1
            time.sleep(1)
    return None

def extract_link_for_asbd(html_content):
    position=9
    # Parse the HTML file
    
    soup = BeautifulSoup(html_content, 'html.parser')

    # Find all script tags with 'src' attribute
    script_tags = soup.find_all('script', attrs={'src': True})

    # Check if the desired position is valid within the range of found scripts
    if position < len(script_tags):
        # Extract the URL at the specified position
        url = script_tags[position].get('src')
        #take away everything after the ?
        url=url.split('?')[0]
        
        return url
    else:
        return None  # No script at this position
